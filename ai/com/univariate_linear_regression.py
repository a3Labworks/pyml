'''
Created on Oct 8, 2018

Content
======
Code that demonstrates how to implement univariate linear regression.
Will also demonstrate the use of tf.summary.scalar and tf.summary.image

How to visualize tensorboard:
=============================
1) start tensorboard by typing in the command line the following:
    tensorboard --logdir=<parent location of project>/pymlgraphs/uni_linear_reg
    
    Example: if you cloned the repo to D:/workspace/pyML, then write:
    tensorboard --logdir=D:/workspace/pymlgraphs/uni_linear_reg
    
2) open your browser at http://localhost:6006/

Exercises:
==========
1) Implement a shuffle data split into test and train datasets, and replace the sklearn dependency
2) HARD: display all image horizontally instead of vertically (plot an array of images) 

@author: alecu
'''

import tensorflow as tf
from sklearn import model_selection
import shutil
import os
from pathlib import Path
import ai.com.utils.graphs as gp

class flags(object):
    def __init__(self,**kwargs):
        self.__dict__.update(kwargs)

def read_input(filename):
    text = open(filename, 'r').readlines()[1:]
    data = [line[:-1].split('\t') for line in text]
    x = [float(line[1]) for line in data]
    y = [float(line[2]) for line in data]
    return x, y


def main(_):
    
    print('Storing logs in: %s' % FLAGS.log_dir)

    ### Prepare logs directory
    if os.path.isdir(FLAGS.log_dir):
        print('Found previous log files. Deleting...')
        shutil.rmtree(FLAGS.log_dir)
    
    # Step 1: read in data from the file
    data_x, data_y = read_input(Path(os.getcwd()).parent.parent / "data/birth.txt")

    # Step 2: split into train and test sets
    train_x, test_x, train_y, test_y = model_selection.train_test_split(data_x, data_y, test_size=0.20, random_state=42)

    sess = tf.InteractiveSession()

    # Step 3: create placeholders for X, Y, W, b
    with tf.name_scope('input'):
        X = tf.placeholder(tf.float32, name='x-input')
        Y = tf.placeholder(tf.float32, name='y-input')

    with tf.name_scope('weights'):
        weight = tf.Variable(tf.truncated_normal([1], stddev=0.1))
        with tf.name_scope('biases'):
            bias = tf.Variable(tf.constant(0.1, shape=[1]))

    # Step 4: build model to predict Y
    Y_predicted = weight * X + bias

    # Step 5: use the squared error as the loss function
    with tf.name_scope('loss'):
        loss = tf.reduce_mean(tf.squared_difference(Y, Y_predicted))
        tf.summary.scalar('loss', loss)

    # Step 6: use graident descent as a training step
    with tf.name_scope('train'):
        train_step = tf.train.GradientDescentOptimizer(FLAGS.learning_rate).minimize(loss)
            
    # setting up the necessary tensors for image plotting
    with tf.name_scope('results'):
        plot_buf_ph = tf.placeholder(tf.string, name='tb_images')      


    # Merge all the summaries and write them out to
    merged = tf.summary.merge_all()
    train_writer = tf.summary.FileWriter(str(FLAGS.log_dir / "train"), sess.graph)
    test_writer = tf.summary.FileWriter(str(FLAGS.log_dir / "test"))
    tf.global_variables_initializer().run()   

    # for a number of epochs, train the model
    for epoch in range(FLAGS.max_steps):
        
        # Record summaries and test-set accuracy
        if epoch % 10 == 0:         
            summary, ls = sess.run([merged, loss], feed_dict={X: test_x, Y: test_y})
            test_writer.add_summary(summary, epoch)
            print('MSE at epoch %s: %s' % (epoch, ls))

        # Record train set summaries, and train
        else:
            summary, _ = sess.run([merged, train_step], feed_dict={X: train_x, Y: train_y})
            train_writer.add_summary(summary, epoch)
        
        # plot the fitted model on test data
        if (epoch % (FLAGS.max_steps/FLAGS.model_plots) == 0) or (epoch == FLAGS.max_steps - 1):
            summary, Y_predicted_ = sess.run([merged, Y_predicted], feed_dict={X: test_x, Y: test_y})
            plot_buf = gp.get_scatterplot_buf(test_x, test_y, test_x, Y_predicted_)            
            image = tf.image.decode_png(plot_buf_ph, channels=4)
            image = tf.expand_dims(image, 0) # make it a batch of 1 image with shape [1, height, width, channels]       
            plot_image_op = tf.summary.image('fitted model', image, max_outputs = 1)
            plot_image_summary = sess.run(plot_image_op, feed_dict={plot_buf_ph: plot_buf.getvalue()})
            test_writer.add_summary(plot_image_summary, global_step=epoch)  
    
    # close the writers
    train_writer.close()
    test_writer.close()
    
    sess.close()
    
    
if __name__ == '__main__':
    FLAGS = flags(max_steps = 1000, learning_rate = 0.01, model_plots = 4, log_dir = Path(os.getcwd()).parent.parent.parent / "pymlgraphs/uni_linear_reg")    
    tf.app.run(main=main)        
    

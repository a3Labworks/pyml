'''
Created on Oct 8, 2018

Content
======
Code that demonstrates how to implement multivariate linear regression.
Will also demonstrate the use of tf.summary.histogram
Will also demonstrate the use of stacked tf.summary.image plots 

How to visualize tensorboard:
=============================
1) start tensorboard by typing in the command line the following:
    tensorboard --logdir=<parent location of project>/pymlgraphs/multi_linear_reg
    
    Example: if you cloned the repo to D:/workspace/pyML, then write:
    tensorboard --logdir=D:/workspace/pymlgraphs/multi_linear_reg
    
2) open your browser at http://localhost:6006/

Exercises:
==========
1) What conclusion can you draw from the "residuals vs fit" plots?
2) Check if you can get better results using a subset of the predictors
3) Check if you can get better results by transforming some of the predictors or by changing the hypothesis


Note:
=====
Theory says that R2 should be between 0 and 1. However, it is still possible to obtain negative values:
- An R2 of 1.0 is the best. It means you have no error in your regression.
- An R2 of 0 means your regression is no better than taking the mean value, i.e. you are not using any information from the other variables
- A Negative R2 means you are doing worse than the mean value.


@author: alecu
'''

import tensorflow as tf
import numpy as np

from sklearn.datasets import load_boston
from sklearn import preprocessing
from sklearn import model_selection
from pathlib import Path

import shutil
import os

import ai.com.utils.graphs as gp


class flags(object):
    def __init__(self,**kwargs):
        self.__dict__.update(kwargs)

# function to read boston dataset from sklearn kit
def read_boston_data():
    boston = load_boston()
    data_x = np.array(boston.data)
    data_y = np.array(boston.target)
    return data_x, data_y

# function to normalize each feature independently
def feature_normalize(dataset):
    normalized_dataset = preprocessing.StandardScaler().fit_transform(dataset)
    return normalized_dataset

# function to add a bias term and reshape a vector to a vector of vectors
def bias_reshape(features,labels):
    n_training_samples = features.shape[0]
    biased_features = np.c_[np.ones(n_training_samples),features] # add a bias term "1" to every sample
    n_features = biased_features.shape[1]    
    reshaped_features = biased_features.reshape(-1, n_features) # reshape to 1-hot encodings
    reshaped_labels = labels.reshape(-1, 1) # reshape to 1-hot encodings
    return reshaped_features, reshaped_labels

# split the data into training and test sets
def train_test_split(features, labels):
    train_x, test_x, train_y, test_y = model_selection.train_test_split(features, labels, test_size=0.20, random_state=42)
    return train_x, test_x, train_y, test_y


def main(_):
    
    print('Storing logs in: %s' % FLAGS.log_dir)

    ### Prepare logs directory
    if os.path.isdir(FLAGS.log_dir):
        print('Found previous log files. Deleting...')
        shutil.rmtree(FLAGS.log_dir)
    

    # read in data from the file
    data_x, data_y = read_boston_data()
    
    # normalize features
    data_x = feature_normalize(data_x)
    
    # add bias term to weight matrix
    data_x, data_y = bias_reshape(data_x, data_y)
    
    # split into train and test sets
    train_x, test_x, train_y, test_y = model_selection.train_test_split(data_x, data_y, test_size=0.20, random_state=42)
 
    sess = tf.InteractiveSession()
    
    with tf.name_scope('input'):
        X = tf.placeholder(tf.float32, [None, data_x.shape[1]], name='x-input')
        Y = tf.placeholder(tf.float32, [None, 1], name='y-input')

    weights = tf.Variable(tf.truncated_normal([data_x.shape[1], 1], stddev=0.1), name="weights")

    # build model to predict Y
    Y_predicted = tf.matmul(X, weights)
    
    with tf.name_scope('metrics'):
        # compute the residual error
        residual = tf.subtract(Y, Y_predicted)
        tf.summary.histogram('residual', residual)
        # Use the squared error as the loss function
        loss = tf.reduce_mean(tf.squared_difference(Y, Y_predicted))
        tf.summary.scalar('loss', loss)
        # R_squared computes the coefficient of determination.
        SSresidual = tf.reduce_sum(tf.squared_difference(Y, Y_predicted))
        SStotal = tf.reduce_sum(tf.squared_difference(Y, tf.reduce_mean(Y)))
        r2 = tf.subtract(1.0, tf.div(SSresidual, SStotal))
        tf.summary.scalar('R2', r2)
 
    # use graident descent as a training step
    with tf.name_scope('train'):
        train_step = tf.train.GradientDescentOptimizer(FLAGS.learning_rate).minimize(loss)
        
    # setting up the necessary tensors for image plotting
    with tf.name_scope('plots'):
        plot_buf_ph = tf.placeholder(tf.string, name='predicted')
        plot_buf_ph2 = tf.placeholder(tf.string, name='residuals')             

    # Merge all the summaries and write them out to
    merged = tf.summary.merge_all()
    train_writer = tf.summary.FileWriter(str(FLAGS.log_dir / "train"), sess.graph)
    test_writer = tf.summary.FileWriter(str(FLAGS.log_dir / "test"))
    tf.global_variables_initializer().run()   

    # for a number of epochs, train the model
    for epoch in range(FLAGS.max_steps):
        
        # Record summaries and test-set accuracy
        if epoch % 10 == 0:         
            summary, ls = sess.run([merged, loss], feed_dict={X: test_x, Y: test_y})
            test_writer.add_summary(summary, epoch)
            print('MSE at epoch %s: %s' % (epoch, ls))
 
        # Record train set summaries, and train
        else:
            summary, _ = sess.run([merged, train_step], feed_dict={X: train_x, Y: train_y})
            train_writer.add_summary(summary, epoch)
            
        # plot the predictions
        if (epoch % (FLAGS.max_steps/FLAGS.model_plots) == 0) or (epoch == FLAGS.max_steps - 1):
            summary, Y_predicted_ = sess.run([merged, Y_predicted], feed_dict={X: test_x, Y: test_y})
            
            plot_buf = gp.get_residual_buf(test_y, Y_predicted_)
            image = tf.image.decode_png(plot_buf_ph, channels=4)
            residuals = test_y - Y_predicted_
            plot_buf2 = gp.get_scatterplot1_buf(Y_predicted_, residuals)
            image2 = tf.image.decode_png(plot_buf_ph2, channels=4)
            im = tf.stack([image, image2], axis=0) # make it a batch of 2 images with shape [2, height, width, channels]       
            plot_image_op = tf.summary.image('residuals', im, max_outputs = 2)
            plot_image_summary = sess.run(plot_image_op, feed_dict={plot_buf_ph2: plot_buf2.getvalue(), plot_buf_ph: plot_buf.getvalue()})
            test_writer.add_summary(plot_image_summary, global_step=epoch)  

                
            
    # close the writers
    train_writer.close()
    test_writer.close()
        
    
    sess.close()

    

if __name__ == '__main__':
    FLAGS = flags(max_steps = 400, learning_rate = 0.01, model_plots = 4, log_dir = Path(os.getcwd()).parent.parent.parent / "pymlgraphs/multi_linear_reg")    
    tf.app.run(main=main)       
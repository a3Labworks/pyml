'''
Created on Oct 8, 2018

Content
======
Code that demonstrates the visual improvement of using graph name scopes
Will also demonstrate the usage of multiple independent graphs

How to visualize tensorboard:
=============================
1) start tensorboard by typing in the command line the following:
    tensorboard --logdir=<parent location of project>/pymlgraphs/graph_naming
    
    Example: if you cloned the repo to D:/workspace/pyML, then write:
    tensorboard --logdir=D:/workspace/pymlgraphs/graph_naming
    
2) open your browser at http://localhost:6006/

@author: alecu
'''

import tensorflow as tf
import shutil
import os
from pathlib import Path

class flags(object):
    def __init__(self,**kwargs):
        self.__dict__.update(kwargs)

def read_input(filename):
    text = open(filename, 'r').readlines()[1:]
    data = [line[:-1].split('\t') for line in text]
    x = [float(line[1]) for line in data]
    y = [float(line[2]) for line in data]
    return x, y


def main(_):
    
    print('Storing logs in: %s' % FLAGS.log_dir)

    ### Prepare logs directory
    if os.path.isdir(FLAGS.log_dir):
        print('Found previous log files. Deleting...')
        shutil.rmtree(FLAGS.log_dir)
    
    g_1 = tf.Graph()
    with g_1.as_default():

        sess_1 = tf.Session()

        with tf.name_scope('input'):
            X = tf.placeholder(tf.float32, name='x-input')
            Y = tf.placeholder(tf.float32, name='y-input')
    
        with tf.name_scope('weights'):
            weight = tf.Variable(tf.truncated_normal([1], stddev=0.1))
            with tf.name_scope('biases'):
                bias = tf.Variable(tf.constant(0.1, shape=[1]))
    
        Y_predicted = weight * X + bias
    
        with tf.name_scope('loss'):
            loss = tf.reduce_mean(tf.squared_difference(Y, Y_predicted))
            tf.summary.scalar('loss', loss)
    
        with tf.name_scope('train'):
            train_step = tf.train.GradientDescentOptimizer(FLAGS.learning_rate).minimize(loss)
            

        # Merge all the summaries and write them out to
        train_writer = tf.summary.FileWriter(str(FLAGS.log_dir / "graph1"), sess_1.graph)
        tf.global_variables_initializer().run(session = sess_1)   
    
        # close the writers
        train_writer.close()
        
        sess_1.close()
 
 
    g_2 = tf.Graph()
    with g_2.as_default():

        sess_2 = tf.Session()

        X = tf.placeholder(tf.float32)
        Y = tf.placeholder(tf.float32)
    
        weight = tf.Variable(tf.truncated_normal([1], stddev=0.1))
        bias = tf.Variable(tf.constant(0.1, shape=[1]))
    
        Y_predicted = weight * X + bias
    
        loss = tf.reduce_mean(tf.squared_difference(Y, Y_predicted))
        tf.summary.scalar('loss', loss)
    
        train_step = tf.train.GradientDescentOptimizer(FLAGS.learning_rate).minimize(loss)
            
        train_writer = tf.summary.FileWriter(str(FLAGS.log_dir / "graph2"), sess_2.graph)
        tf.global_variables_initializer().run(session = sess_2)   
    
        # close the writers
        train_writer.close()
        
        sess_2.close()
       
    
    
if __name__ == '__main__':
    FLAGS = flags(learning_rate = 0.01, log_dir = Path(os.getcwd()).parent.parent.parent / "pymlgraphs/graph_naming")    
    tf.app.run(main=main)        
    

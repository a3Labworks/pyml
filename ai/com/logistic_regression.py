'''
Created on Oct 8, 2018

Content
======
Code that demonstrates how to implement logistic regression.
Will also demonstrate batch training
Will also demonstrate the use of tf.summary.image

How to visualize tensorboard:
=============================
1) start tensorboard by typing in the command line the following:
    tensorboard --logdir=<parent location of project>/pymlgraphs/log_reg
    
    Example: if you cloned the repo to D:/workspace/pyML, then write:
    tensorboard --logdir=D:/workspace/pymlgraphs/log_reg
    
2) open your browser at http://localhost:6006/

Exercises:
==========
1) Implement a hyperparameter grid search to find the best performance (accuracy) for the parameters:
- optimizer (comparing Gradient Descent and Adam, maybe use others from tf.train)
- learning rate
- batch size


@author: alecu
'''

import tensorflow as tf

from tensorflow.examples.tutorials.mnist import input_data
from pathlib import Path

import shutil
import os


class flags(object):
    def __init__(self,**kwargs):
        self.__dict__.update(kwargs)


def main(_):
    
    print('Storing logs in: %s' % FLAGS.log_dir)

    ### Prepare logs directory
    if os.path.isdir(FLAGS.log_dir):
        print('Found previous log files. Deleting...')
        shutil.rmtree(FLAGS.log_dir)
    

    # download data and read, using TF Learn's built in function to load MNIST data
    # read labels as class (integers) not as on-hot, since sparse_softmax_cross_entropy works only for class variables
    mnist = input_data.read_data_sets('D:/workspace/pyml/data/mnist', one_hot=False)

    print("...Finished downloading data")
 
    sess = tf.InteractiveSession()
    
    with tf.name_scope('input'):
        X = tf.placeholder(tf.float32, [None, 784], name='image') 
        Y = tf.placeholder(tf.int64, [None], name='label')
        
    with tf.name_scope('weights'):
        weights = tf.Variable(tf.truncated_normal([784, 10], stddev=0.1), name="weights")
        biases = tf.Variable(tf.constant(0.1, shape=(1, 10)), name='biases')

    # build model to predict Y
    Y_predicted = tf.matmul(X, weights) + biases         
 
    with tf.name_scope('cross_entropy'):
        with tf.name_scope('total'):
            cross_entropy = tf.losses.sparse_softmax_cross_entropy(labels=Y, logits=Y_predicted)
            loss = tf.reduce_mean(cross_entropy) # computes the mean over all the examples in the batch
    tf.summary.scalar('cross_entropy', cross_entropy)
 
    # use graident descent as a training step
    with tf.name_scope('train'):
        train_step = tf.train.GradientDescentOptimizer(FLAGS.learning_rate).minimize(loss)
        #train_step = tf.train.AdamOptimizer(FLAGS.learning_rate).minimize(loss)

    # compute performance metrics
    with tf.name_scope('accuracy'):
        with tf.name_scope('correct_prediction'):
            correct_prediction = tf.equal(tf.argmax(Y_predicted, 1), Y)
        with tf.name_scope('accuracy'):
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    tf.summary.scalar('accuracy', accuracy)
        
        
    # Merge all the summaries and write them out to
    merged = tf.summary.merge_all()
    train_writer = tf.summary.FileWriter(str(FLAGS.log_dir / "train"), sess.graph)
    test_writer = tf.summary.FileWriter(str(FLAGS.log_dir / "test"))
    tf.global_variables_initializer().run()   


    test_x, test_y = mnist.test.images, mnist.test.labels
    
    # for a number of epochs, train the model
    for epoch in range(FLAGS.max_steps):
        
        
        # Record summaries and test-set accuracy
        if epoch % 10 == 0:         
            summary, ls = sess.run([merged, accuracy], feed_dict={X: test_x, Y: test_y})
            test_writer.add_summary(summary, epoch)
            print('Accuracy at epoch %s: %s %%' % (epoch, ls))
 
        # Record train set summaries, and train
        else:
            train_x, train_y = mnist.train.next_batch(FLAGS.batch_size)
            
            summary, _ = sess.run([merged, train_step], feed_dict={X: train_x, Y: train_y})
            train_writer.add_summary(summary, epoch)
            
        if epoch == FLAGS.max_steps - 1:
            
            image_shaped_input = tf.reshape(test_x, [-1, 28, 28, 1])
            plot_image_op = tf.summary.image('input', image_shaped_input, FLAGS.model_plots)
            summary, y_, plot_image_summary = sess.run([merged, Y_predicted, plot_image_op], feed_dict={X: test_x, Y: test_y})
            predicted_class = tf.argmax(y_, 1)
            test_writer.add_summary(plot_image_summary, epoch)            
            print("Test Labels    :    ", test_y[0:FLAGS.model_plots])
            print("Test Prediction:    ", predicted_class[0:FLAGS.model_plots].eval())
                    
                
            
    # close the writers
    train_writer.close()
    test_writer.close()
        
    
    sess.close()

    

if __name__ == '__main__':
    FLAGS = flags(max_steps = 1000, learning_rate = 0.01, model_plots = 10, batch_size = 128, log_dir = Path(os.getcwd()).parent.parent.parent / "pymlgraphs/log_reg")    
    tf.app.run(main=main)       
'''
Created on Oct 10, 2018

@author: alecu
'''

import matplotlib.pyplot as plt
import io as io

def get_scatterplot_buf(x1, y1, x2, y2):
    plt.figure()

    plt.plot(x1, y1, 'bo', label='Real data')
    plt.plot(x2, y2, 'r', label='Predicted data')
    plt.legend()

    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    plt.close()

    buf.seek(0)
    return buf

def get_residual_buf(actual_y, predicted_y):
    fig, ax = plt.subplots()
    
    ax.scatter(actual_y, predicted_y, label="Predicted")
    ax.plot([actual_y.min(), actual_y.max()], [actual_y.min(), actual_y.max()], 'k--', lw=3, label='Ideal')
    ax.set_xlabel('Real Y')
    ax.set_ylabel('Predicted Y')
    ax.legend()

    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    plt.close()

    buf.seek(0)
    return buf

def get_scatterplot1_buf(x, y):
    fig, ax = plt.subplots()
    
    ax.scatter(x, y)
    ax.set_xlabel('Fits (predicted)')
    ax.set_ylabel('Residuals')

    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    plt.close()

    buf.seek(0)
    return buf